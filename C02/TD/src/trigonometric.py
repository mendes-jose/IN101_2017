from factorial import factorial 

def cosine(x):
    '''compute the cosine of x.
    uses a Taylor series (10 first steps only).
    Input: x, the number for which to compute the cosine
    Output: the cosine of x
    '''
    result = 0
    for n in range(10):
        result = result + ( ( ((-1)**n) * (x**(2*n)) ) / factorial(2*n) )
    return result


def sine(x):
    '''compute the sine of x.
    uses a Taylor series (10 first steps only).
    Input: x, the number for which to compute the sine
    Output: the sine of x
    '''
    result = 0
    for n in range(10):
        result = result + ( ( ((-1)**n) * (x**(2*n + 1)) ) / factorial(2*n+1) )
    return result


def tangent(x):
    '''compute the tangent of x.
    Input: x, the number for which to compute the sine
    Output: the tangent of x
    '''
    return sine(x) / cosine(x)



if __name__ == "__main__":
    
    # import plotting functions
    from matplotlib.pylab import scatter, show
    
    # plot the tangent for all values between -1.5 and 1.5
    x = -1.50
    while (x < 1.50):
        scatter(x, tangent(x))
        x = x + 0.02
      
    # show graphical plot
    show()
