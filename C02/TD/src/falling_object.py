# import plotting functions
from matplotlib.pylab import scatter, show

def height(t):
    '''Compute the height of an object falling from 4414.5 meters.
    Input: time 't' the object is already falling
    Output: height of the object at time 't'
    '''
    return 4414.5 - (9.81 * t**2) / 2

if __name__ == "__main__":
    # plot the height of the object every 2 seconds 
    # until it hits the ground after 30 seconds
    for t in range(0, 30, 2):
        scatter(10, height(t))

    # show graphical plot
    show()
