#!/usr/bin/env python

def gcd(a, b):
	assert(a > 0 and b > 0), 'gcd can only be computed for numbers > 0'

	if a == b:
		return int(a)
	elif a > b:
		return gcd(a-b, b)
	else:
		return gcd(a, b-a)

def testGcd():
	assert(gcd(1,1) == 1)
	assert(gcd(7,7) == 7)
	assert(gcd(7,9) == 1)
	assert(gcd(15,12) == 3)

if __name__ == "__main__":
	print ('In main', gcd(7,9))
	print ('In main', gcd(3,3))
	print ('In main', gcd(6,2))
	testGcd()