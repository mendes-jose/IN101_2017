def edit_distance_simple(x,y,i=None,j=None):
    ''' Compute the edit distance between two strings.
    
    See also: https://en.wikipedia.org/wiki/Edit_distance
    
    The penalty for a replacement is 1, and for a gap it is 2.
    
    The implementation was inspired by the recursive algorithm suggested here: 
    http://www.cs.princeton.edu/courses/archive/fall09/cos126/assignments/sequence.html
    
    Inputs:
        x, y - Two strings to be compared
        i, j - Current indices in the string
        
        Personally, I prefer longer variable names. But I have kept the short 
        ones for consistency with the notation used in the second link above.
        
    Output: 
        The edit distance between x and y.
        
    If i and j are None (the defaults), they will be set to the last indices in
    the two strings.    
    '''

    # If i and/or j are None, set them to the last index of the string
    if i==None:
        i = len(x)-1
    if j==None:
        j = len(y)-1

    #--------------------------------
    # case 0: before the first index in one or both of the sequences
    if i<0 and j<0:
        # Distance between two empty strings is zero.
        return 0
    
    #--------------------------------
    # case 1: align x[i] with y[j], check if they are different or not
    if i>=0 and j>=0: # Matching can only be done when indices still in strings
        penalty_match = 1 # Assume they are different
        if (x[i]==y[j]):
            penalty_match = 0 # If they are the same, no penalty
        # Get the ED for the rest of the strings, add current penalty
        ed_match = edit_distance_simple(x,y,i-1,j-1) + penalty_match
    else:
        # The edit distance for matching could not be computed. Set it to the 
        # largest possible value so that it will never be the minimum.
        ed_match = float('inf') # Note: int('inf') does not exist

    #--------------------------------
    # case 2: assume gap in x, penalize with 2
    if j>=0: # Adding gap to x can only be done if y is not empty
        ed_gap_x = edit_distance_simple(x,y,i,j-1) + 2
    else:
        ed_gap_x = float('inf')
        
    #--------------------------------
    # case 3: assume gap in x, penalize with 2
    if i>=0: # Adding gap to y can only be done if x is not empty
        ed_gap_y = edit_distance_simple(x,y,i-1,j) + 2
    else:
        ed_gap_y = float('inf')
    
    # The edit distance is the minimum of the result of case 1, 2 or 3
    return min([ed_match, ed_gap_x, ed_gap_y])
    

def edit_distance(x,y,i=None,j=None):
    ''' Compute the edit distance between two strings.
    
    See also: https://en.wikipedia.org/wiki/Edit_distance
    
    The penalty for a replacement is 1, and for a gap it is 2.
    
    The implementation was inspired by the recursive algorithm suggested here: 
    http://www.cs.princeton.edu/courses/archive/fall09/cos126/assignments/sequence.html
    
    Inputs:
        x, y - Two strings to be compared
        i, j - Current indices in the string
        
        Personally, I prefer longer variable names. But I have kept the short 
        ones for consistency with the notation used in the second link above.
        
    Output: 
        A 4-tuple containing:
            * The edit distance between x and y
            * The aligned version of string x 
            * The aligned version of string y
            * A string representing the penalties at each index
        
    If i and j are None (the defaults), they will be set to the last indices in
    the two strings.    
    
    '''

    # If i and/or j are None, set them to the last index of the string
    if i==None:
        i = len(x)-1
    if j==None:
        j = len(y)-1
        
    #--------------------------------
    # case 0: before the first index in one or both of the sequences
    if i<0 and j<0:
        # Distance between two empty strings is zero.
        return (0,'','','')
    
    
    #--------------------------------
    # case 1: align x[i] with y[j], check if they are different or not    
    if i>=0 and j>=0: # Matching can only be done when indices still in strings
        penalty_match = 1 # Assume they are different
        if (x[i]==y[j]):
            penalty_match = 0 # If they are the same, no penalty
        # Get the edit_distance for the rest of the strings
        (ed_match,x_match,y_match,penalties_match) = edit_distance(x,y,i-1,j-1)
        ed_match += penalty_match
    else:
        ed_match = float('inf') # Note: int('inf') does not exist
        

    #--------------------------------
    # case 2: assume gap in x
    # x_gap_y means: aligned string 'x', when assuming gap in 'y'
    if j>=0: # Adding gap to x can only be done if y is not empty
        (ed_gap_x,x_gap_x,y_gap_x,penalties_gap_x) = edit_distance(x,y,i,j-1)
        ed_gap_x += 2
    else:
        ed_gap_x = float('inf')
    
    #--------------------------------
    # case 3: assume gap in y
    # y_gap_y means: aligned string 'y', when assuming gap in 'y'
    if i>=0: # Adding gap to y can only be done if x is not empty
        (ed_gap_y,x_gap_y,y_gap_y,penalties_gap_y) = edit_distance(x,y,i-1,j)
        ed_gap_y += 2
    else:
        ed_gap_y = float('inf')
    
    
    #--------------------------------
    # Get the smallest edit distance over the 3 cases
    # Whichever is smallest will determine which variables are returned
    ed_min = min([ed_match, ed_gap_x, ed_gap_y])
    
    if ed_match==ed_min:
        p = str(penalty_match) # Convert to string
        return (ed_match,x_match+x[i],y_match+y[j],penalties_match+p)

    elif ed_gap_x==ed_min:
        return (ed_gap_x, x_gap_x+x[i], y_gap_x+'-', penalties_gap_x+'2')

    else: # ed_gap_y==ed_min
        return (ed_gap_y, x_gap_y+'-', y_gap_y+y[j], penalties_gap_y+'2')



def _test_edit_distance():
    assert edit_distance_simple('','')==0, 'Edit distance wrong...'
    assert edit_distance_simple('GCAT','GCAT')==0, 'Edit distance wrong...'
    assert edit_distance_simple('AT','AC')==1, 'Edit distance wrong...'
    assert edit_distance_simple('AT','A')==2, 'Edit distance wrong...'
    assert edit_distance_simple('GCATTCA','GTATC')==5, 'Edit distance wrong...'
    print('All edit_distance tests were a success!')


if __name__ == "__main__":
    _test_edit_distance()

    # This is not a test, but more to visualize the result of the alignment
    dna1 = 'GCATTCA'
    dna2 = 'GTATC'
    (edit_dist,x_aligned,y_aligned,penalties) = edit_distance(dna1,dna2)
    print('Inputs:')
    print('    DNA1 = '+dna1)
    print('    DNA2 = '+dna2)
    print('Outputs:')
    print('    edit distance =',edit_dist)
    print('    '+x_aligned)
    print('    '+y_aligned)
    print('    '+penalties+' => '+str(edit_dist))

