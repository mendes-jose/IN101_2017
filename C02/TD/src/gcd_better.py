#!/usr/bin/env python3
# -*- coding: utf-8 -*-

def gcd(a, b):
    """Calculate the Greatest Common Divisor of a and b.

    Unless b==0, the result will have the same sign as b (so that when
    b is divided by it, the result comes out positive).
    """
    while b:
        a, b = b, a%b
    return a

def _test_gcd():
    ''' Function for testing the gcd function'''
    assert (gcd(12,16)==4), 'GCD test failed'
    assert (gcd(12,2)==2), 'GCD test failed'
    assert (gcd(9,27)==9), 'GCD test failed'
    assert (gcd(10,10)==10), 'GCD test failed'

    # Here come an advanced trick to see if gcd is checking whether a > 0
    try:
        # This should cause an assertion error, which is caught below.
        c = gcd(-1,5)
        print("The line above should have throw an AssertionError, but it didn't.")
    except AssertionError:
        # Our invalid call of gcd cause an error. That's a good thing.
        print('All gcd tests were successful!')
        pass



if __name__ == "__main__":
    # Uncomment the next line if you want to run some tests.
    _test_gcd()

    # import plotting functions
    from matplotlib.pylab import scatter, show

    # plot the gcd of all pairs of numbers between 1 and 20
    for a in range(1, 20):
        for b in range(1, 20):
            scatter(a, gcd(a, b))

    # show graphical plot
    show()
