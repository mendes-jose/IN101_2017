def factorial(n):
    '''Compute the factorial of n.
    
    Input:  n, integer of which to compute factorial (n>=0) 
    Output: factorial of n, i.e. n!
    '''
    assert(n>=0),'Cannot compute factorial of negative number '+str(n)
    
    # Start at 1, and multiple with 2,3,4 .. n
    result = 1
    for i in range(2, n + 1):
        result *= i 
    
    return result
    
    
def _test_factorial():
    ''' Function for testing the factorial function'''
    assert (factorial(0)==1), 'Factorial test failed'
    assert (factorial(1)==1), 'Factorial test failed'
    assert (factorial(2)==2), 'Factorial test failed'
    assert (factorial(3)==6), 'Factorial test failed'
    print('All factorial tests were successful!')

if __name__ == "__main__":
    _test_factorial()
