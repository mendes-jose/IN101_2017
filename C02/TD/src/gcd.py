def gcd(a,b):
    '''compute the greatest common divisor of a and b.
    Inputs: a,b are two positive integer numbers
    Output: greatest common divisor of a and b
    
    Assumes a and b are larger than 0, otherwise the recursion will not terminate.
    '''
    
    assert(a>0 and b>0), 'gcd can only be computed for numbers > 0'
    
    # This is Euclid's algorithm
    if a == b:
        return a
    elif a > b:
        return gcd(a-b, b) # Recursive call
    else:
        return gcd(a, b-a) # Recursive call
        
        
def _test_gcd():
    ''' Function for testing the gcd function'''
    assert (gcd(12,16)==4), 'GCD test failed'
    assert (gcd(12,2)==2), 'GCD test failed'
    assert (gcd(9,27)==9), 'GCD test failed'
    assert (gcd(10,10)==10), 'GCD test failed'
    
    # Here come an advanced trick to see if gcd is checking whether a > 0 
    try:
        # This should cause an assertion error, which is caught below.
        c = gcd(-1,5) 
        print("The line above should have throw an AssertionError, but it didn't.")
    except AssertionError:
        # Our invalid call of gcd cause an error. That's a good thing.
        pass
        
    print('All gcd tests were successful!')
    

if __name__ == "__main__":
    # Uncomment the next line if you want to run some tests.
    _test_gcd()
    
    # import plotting functions
    from matplotlib.pylab import scatter, show

    # plot the gcd of all pairs of numbers between 1 and 20
    for a in range(1, 20):
        for b in range(1, 20):
            scatter(a, gcd(a, b))
    
    # show graphical plot
    show()
  
