def modulo(dividend, divisor):
    '''Computes the modulo, i.e. the remainder after division.
   
   "dividend" must be >=0 and "divisor" >=1. If not, and error is signaled.
   
    inputs:
        dividend: the number that will be divided 
        divisor: the number that will be used o divide the dividend
        
    return:
        the remainder after division
    '''
    
    assert (dividend>=0), 'dividend must be positive'
    assert (divisor>=1), 'dividend must be 1 or larger'
    
    # initially assume that the remainder is equal to the dividend
    remainder = dividend

    # repeatedly subtract the divisor from the remainder until the result is 
    # smaller then the divisor.
    while (divisor <= remainder):
        remainder = remainder - divisor 

    # return the remainder
    return remainder


def _test_modulo():
    ''' Function for testing the modulo function'''
    assert (modulo(6,4)==2), 'Modulo test failed'
    assert (modulo(6,3)==0), 'Modulo test failed'
    assert (modulo(5,4)==1), 'Modulo test failed'
    assert (modulo(0,5)==0), 'Modulo test failed'
    print('All modulo tests were successful!')


if __name__ == "__main__":
    # Call _test_modulo to run some tests
    _test_modulo()
