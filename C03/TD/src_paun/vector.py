from array import *

def vector_add(a,b):
    """Add two vectors and return the result.

        The length of the result vector is equal to the length of the
        shorter input vector.
    """

    # determine the length of the smaller vector
    length = len(a)
    if (len(a) > len(b)):
        length = len(b)

    # create the result array (with the proper length)
    result = [0] * length

    # add the elements of the two vectors and store the result
    result = [a[i] + b[i] for i in range(length)]

    # return the result
    return array('i', result)

def _test_vector_add():
    assert (vector_add(array('i', [0, 1, 2, 3, 4]),
                       array('i', [0, 1, 2, 3, 4, 5, 6, 7, ])) ==
            array('i', [0, 2, 4, 6, 8])), "Actual addition failed."
    assert (vector_add(array('i', []), array('i', [])) ==
            array('i', [])), "Addition of empty vectors failed."

    # all seems fine, so print a message
    print("All tests succeeded.")


if __name__ == "__main__":
    # test the vector addition
    _test_vector_add()
