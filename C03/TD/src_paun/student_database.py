def get_students():
    """Retrieve a mapping from student logins to names."""
    return {'klarsen':'Kim G. Larsen',
            'dbjorner':'Dines Bjorner',
            'hnielsen':'Hanne Ries Nielsen'}

def get_courses():
    """Retrieve a mapping from course numbers to full course names."""
    return {'IN101':'Algorithmique et Programmation (en Python)',
            'IN102':'Système et Programmation (en C)',
            'IN104':'Projet informatique'}

def get_grades():
    """Retrieve a mapping student logins to grades for a set of courses."""
    return {'klarsen': {'IN101':20,'IN102':20,'IN104':20},
            'dbjorner': {'IN101':20,'IN104':20},
            'hnielsen': {'IN102':20,}}
