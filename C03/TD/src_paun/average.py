def average(list):
    """Return the average over the values of the given list.

       Zero is returned, when the list is empty.
    """
    if (len(list) == 0):
        return 0
    else:
        # compute the total sum over all elements
        total = 0
        for e in list:
            total += e

        # divide the total through the number of elements
        return total/len(list)

def _test_average():
    """Test the average function."""

    # Test the actual average computation.
    assert average([0, 1, 2, 3, 4]) == 2, "Actual average computation failed."
    # Test the corner case of an empty list.
    assert average([]) == 0, "Average of empty list is non-zero."

    # all seems fine, so print a message
    print("All tests succeeded.")


if __name__ == "__main__":
    # test the average function
    _test_average()
