import array

def swap_first_last(list):
    """ Swap the first and last element of a list.

        The list is not modified when its length is smaller then 2.
    """

    # check the list's length -- are the first and last element different?!?
    if (len(list) > 1):
        tmp = list[0]
        list[0] = list[len(list) - 1]
        list[len(list) - 1] = tmp

    # return the list in all cases
    return list

def _test_swap():
    """ Test the swap_first_last function."""
    # test regular case actually performing a swap.
    assert (swap_first_last([0, 1, 2, 3, 4]) ==
            [4,1,2,3,0]), "Actual swap failed."
    # test error case with a list with only one element.
    assert (swap_first_last([0]) ==
            [0]), "List with 1 element failed."
    # test error case with an empty list.
    assert (swap_first_last([]) ==
            []), "List with no element failed."

    # seems the swap function also works on arrays.
    assert (swap_first_last(array.array('i', [0,1,2,3,4])) ==
            array.array('i', [4,1,2,3,0])), "Swap on array failed."

    # all seems fine, so print a message
    print("All tests succeeded.")


if __name__ == "__main__":
    # test the swap function
    _test_swap()
