# import functions
from average import average
from matplotlib.pylab import bar, plot, show
import numpy as np # for using random

def barchart_with_average(aList):
    bar(range(len(aList)), aList)
    # plot([average(aList)] * len(aList))
    plot([0, len(aList)], [average(aList), average(aList)], color='red')

# input: list size and max value
listSize = 10
maxInt = 100

# random integers list with maxInt values from 0 to maxInt
l = np.random.randint(maxInt, size=listSize)

# Compute the average for some example arrays
barchart_with_average(l)

# show graphical plot
show()
