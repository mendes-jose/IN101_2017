list=[1,2,3]
# The for loop on a list gives all *elements*.
for element in list:
    print(element)

dictionary={'dog':1, 'cat':2}
# The for loop on a dictionary gives all *keys*,
for key in dictionary:
    print(key)