from student_database import *

def get_student_id(name, student_db):
    """Get the login of a student given its full name."""

    # Traverse the entire student database
    for login in student_db:
        # check if the name associated with the login matches
        if student_db[login] == name:
            return login

    # well this should not happen
    assert false, "Student not found"

def print_grades_and_courses(login, student_db, course_db, grades_db):
    """Print a student's name and its grades from all courses."""

    # retrieve all the grades associated with the student's login
    all_grades = grades_db[login]

    # print the student's name
    print("Student: ", student_db[login])

    # traverse all the grades associated with the student
    for course_id in all_grades:
        # print the course's name and the grade
        print("  ", course_db[course_id], ":", all_grades[course_id])

if __name__ == "__main__":
    # retrieve the student, course, and grade databases
    student_db=get_students()
    course_db=get_courses()
    grades_db=get_grades()

    # get the student login associated with the name "Kim G. Larsen"
    kim_login = get_student_id("Kim G. Larsen", student_db)

    # print his name and his grades for all courses
    print_grades_and_courses(kim_login, student_db, course_db, grades_db)
