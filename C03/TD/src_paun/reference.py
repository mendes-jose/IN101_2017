import swap

# create a simple list
list=[0,1,2,3,4,5]

# invoke the swap function.
# ATTENTION: pass a copy of the original list instead of the original
# reference.
swapped=swap.swap_first_last(list[:])

# print the original list and the list with the swapped elements.
print(list)
print(swapped)
