def print_dict(offset, dic):
	"""[summary]

	[description]

	Arguments:
		offset {[type]} -- [description]
		dic {[type]} -- [description]
	"""

	for item in dic.items():
		print(offset*' '+'{}='.format(item[0]), end='')
		if isinstance(item[1], dict):
			print('\n'+offset*' '+'{')
			print_dict(offset+2, item[1])
			print(offset*' '+'}')
		else:
			print(item[1])

nested_dict1={'subsubkey1':2}
nested_dict2={'subkey4':'Hallo',5:6,'subkey4':0}
nested_dict3={'subkey1':[0],'subkey2':[1],'subkey3':nested_dict1}
dic={'key1':nested_dict3,'key2':1.5,'key3':nested_dict2}

print_dict(0, dic)
