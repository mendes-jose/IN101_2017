def swap(aList):
	""" Swap first and last elements of a list

	Arguments:
		aList {list} -- list

	Returns:
		list -- list with first and last elements swapped
	"""

	# Decide to throw an error or just return things as they are if not a list is received

	# throw error if argument is not a list (we could just let future operations throw it)
	# assert(isinstance(aList, list)), "{} is not a list".format(type(aList))

	# return unchanged (considering any iterable type as valid)
	if (hasattr(aList, '__iter__') == False):
		return aList

	# for considering only lists use:
	# if (isinstance(aList, list) == False):

	# check if list is not empty, if it is empty retur a empty list
	if (len(aList) > 1):
		# swap
		nList = aList[:] # copy list
		aux = nList[0]
		nList[0] = nList[-1]
		nList[-1] = aux
		return nList

	else:
		return aList

def _testSwap():
	"""test swap
	"""

	assert(swap([]) == [])
	assert(swap([0,1]) == [1,0])
	assert(swap(['hello', 1, 'salut']))

if __name__ == "__main__":
	_testSwap()
	print("All good")
