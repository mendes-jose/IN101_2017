## Dictionaries are the appropriated containter
# 1. creating a map from arbitrary types to arbitrary types without dictionary demands more than one instance of list to accomplish what one instance of dictionary can do
# 2. number 1 entails that the code will be more readable and understandable
#
#
from student_database import *

def getLoginFromName(fullName):
    """Get student's login from his/her full name

    If two students have exacly the same name it returns a list of logins

    Arguments:
        fullName {string} -- student's full name
    """
    studentsDB = get_students()

    logins = []

    for item in studentsDB.items():
        if fullName == item[1]:
            logins += [item[0]]

    if len(logins) == 0:
        return
    elif len(logins) == 1:
        return logins[0]
    else:
        return logins

def getGradesFromStudent(studentLogin):
    """Get student's grades (with name of courses) given his/her login

    It assumes each student has a unique login (after all the sole purpose of logins)

    Arguments:
        studentLogin {string} -- student login
    """

    gradesDB = get_grades()

    return gradesDB[studentLogin]


def main():
    # coursesDB = get_courses()

    fullName = 'Dines Bjorner'

    login = getLoginFromName(fullName)

    print (login)

    print (getGradesFromStudent(login))

    fullName = 'Kim G. Larsen'

    print ("{}'s grades: {}".format(fullName,
        getGradesFromStudent( getLoginFromName(fullName) )))

if __name__ == "__main__":
    main()
