from better_swap import *
# from swap import *
from array import *

l = [1,2,3]

print ("{} -> {}".format(l, swap(l)))

print ("{} -> {}".format(['a','b','c'], swap(['a','b','c'])))

# print ("{} -> {}".format(1, swap(1)))
#
# print ("{} -> {}".format([], swap([])))

# print ("{} -> {}".format(array('i', l), swap(array('i', l))))
#
# print ("{} -> {}".format(array('i', [1,2,3]), swap(array('i', [1,2,3]))))
