## The better choise for representing vectors among list, dict and array data structis is array because:
# 1. accessing data from a vector using numercial index (e.g. vec[3]) is simple and understandable so we can put dictionaries aside
# 2. values in a vector should all have the save type (numerical types such as float) thus lists are clearly an overkill
# 3. arrays are faster and if they suffice for representing vectors we should use them
#

# '* operator' in the exercise's text refers to []*n for creating a n-sized list

from array import *

def addVectors(vec1, vec2):
    """Add 2 vectors

    Arguments:
        vec1 {list, dict or array} -- a vector of elements having + operator
        vec2 {list, dict or array} -- a vector of elements having + operator

    Returns:
        array -- array holding the addition of vec1 and vec2
    """


    # can handle vec1 and vec2 being either list, dict or array

    # arr1 = array('d', vec1.values()) if isinstance(vec1, dict) else arr1 = array('d', vec1)
    # arr2 = array('d', vec2.values()) if isinstance(vec1, dict) else arr2 = array('d', vec1)

    # code above is the same as the following commented code but in one line
    if isinstance(vec1, dict):
        arr1 = array('d', vec1.values())
    else:
        arr1 = array('d', vec1)

    if isinstance(vec2, dict):
        arr2 = array('d', vec2.values())
    else:
        arr2 = array('d', vec2)

    # we are going to generate a vector with dimension equals to max(len(vec1), len(vec2)),
    # align the the first element of vec1 with the first element of vec2, assume zeros if
    # the dimensions do not match and then add

    # this can be done as follows:

    if (len(arr1) < len(arr2)):
        smallerArr = arr1
        biggerArr = arr2
    else:
        smallerArr = arr2
        biggerArr = arr1

    res = smallerArr[:] # copy

    for idx in range(len(smallerArr)):
        res[idx] += biggerArr[idx]

    return res

if __name__ == "__main__":

    v1 = [1.,2., 3.]
    v2 = [2.,1.]
    sumV = addVectors(v1, v2)
    print ('{} + {} = {}'.format(list(v1), list(v2), list(sumV)))

    # print ('[{}] + [{}] = [{}]'.format('  '.join(map(str, v1)),
    #     '  '.join(map(str, v2)), '  '.join(map(str, sumV))))
