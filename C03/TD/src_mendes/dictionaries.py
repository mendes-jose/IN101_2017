
# input
aDict = {'dog':1, 'cat': 2}
aList = [1, 2]

# algorithm
for elemInDict, elemInList in zip(aDict, aList):
    if (elemInDict == elemInList):
        print ('{} is equal to {}. They behave alike.'.format(elemInDict, elemInList))
    else:
        print ('{} is not equal to {}. They do not behave alike.'.format(elemInDict, elemInList))
