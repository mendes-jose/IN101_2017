def mean(aList):
	_sum = aList[0]
	for i in aList[1:]:
		_sum += i

	return _sum/len(aList)

if __name__ == "__main__":

	assert(mean([0,1]) == 0.5)
	print('All good')
