import mean # import my average module
import matplotlib as mpl # import plot librarie as mpl
import matplotlib.pyplot as plt # import pyplot as plt (only for readability)
import numpy as np # for using random

# input: list size and max value
listSize = 10
maxInt = 100

# random integers list with maxInt values from 0 to maxInt
l = np.random.randint(maxInt, size=listSize)

# plot bar graph
plt.bar(range(listSize), l)

# compute mean value
avg = mean.mean(l)

# plot mean value as a red horizontal line
plt.plot([0, listSize], [avg, avg], color='red')

# show it all
plt.show()
