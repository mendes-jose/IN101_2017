# Q1

Algorithm Inorder(tree)
   1. Traverse the left subtree, i.e., call Inorder(left-subtree)
   2. Visit the root.
   3. Traverse the right subtree, i.e., call Inorder(right-subtree)

Algorithm Preorder(tree)
   1. Visit the root.
   2. Traverse the left subtree, i.e., call Preorder(left-subtree)
   3. Traverse the right subtree, i.e., call Preorder(right-subtree)

Algorithm Postorder(tree)
   1. Traverse the left subtree, i.e., call Postorder(left-subtree)
   2. Traverse the right subtree, i.e., call Postorder(right-subtree)
   3. Visit the root.

in-order: [4, 10, 12, 15, 18, 22, 24, 25, 31, 35, 44, 50, 66, 70, 90]

# Q2

```
             2
          /     \
       3           5
     /   \       /   \
   10     8     7    22
   / \   / \   /
  11 13 20 24 16
```

Brute force O(n2): 

```cpp
// function to shuffle an array of size 2n
void shuffleArray(int a[], int n)
{
    // Rotate the element to the left
    for (int i = 0, k = n; i < n; i++, k++)
        for (int j = k; j > 2 * i + 1; j--)
            swap(a[j-1], a[j]);
}
```

Another O(n2) version:

```py
def sw(aList):
    n = int(len(aList)/2)
    for i in range(n):
        for j in range(n-i-1):
            aList[n+i-j], aList[n+i-j-1] = aList[n+i-j-1], aList[n+i-j]
            # print(aList)
    return aList
```

Divide and conquer O(nlogn):

```cpp
// function to shuffle an array of size 2n
void shufleArray(int a[], int f, int l)
{
    // If only 2 element, return
    if (l - f == 1)
        return;
 
    // finding mid to divide the array
    int mid = (f + l) / 2;
 
    // using temp for swapping first half of second array
    int temp = mid + 1;
 
    // mmid is use for swapping second half for first array
    int mmid = (f + mid) / 2;
 
    // Swapping the element
    for (int i = mmid + 1; i <= mid; i++)
        swap(a[i], a[temp++]);
 
    // Recursively doing for first half and second half
    shufleArray(a, f, mid);
    shufleArray(a, mid + 1, l);
}
```

Linear time O(n):

```cpp
void shufleArray(int a[], int n)
{
    n = n / 2;

    for (int start = n + 1, j = n + 1, done = 0, i;
                      done < 2 * n - 2; done++) {
        if (start == j) {
            start--;
            j--;
        }

        i = j > n ? j - n : j;
        j = j > n ? 2 * i : 2 * i - 1;

        swap(a[start], a[j]);
    }
}
```
