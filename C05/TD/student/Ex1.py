##TD5

def dichotom_sachant_k(x, L, k):
    a = k
    b = len(L) -1 + k
    c = ((a+b)//2) % len(L)
    if L[a] == x :
        return a
    elif L[b % len(L)] == x :
        return b
    elif L[c] == x :
        return c
    while A[c] != x :
        if (A[c] > x and A[a]< x) :
            b = c
            c = ((a+b)//2) % len(L)
        elif (A[c]<x and A[b]>x ):
            a = c
            c = ((a+b)//2) % len(L)
        elif L[a] == x :
            return a
        elif L[b % len(L)] == x :
            return b
        elif L[c] == x :
            return c
    return c

if __name__=="__main__" :
    assert(dichotom_sachant_k(5, [1, 2, 3, 4, 5, 6, 7, 8, 9], 0)==4), print("Ne marche pas")
    assert(dichotom_sachant_k(5, [8, 9, 1, 2, 3, 4, 5, 6, 7], 2)==6), print("Ne marche pas")
    print("Tout fonctionne")


