def _create_partial_max_lists(evExcScores):
    """ Given a list it creates a two partial maxima lists from 0 to mid point
        and from mid point to the end of the list.

        Ex :

            input: [10, 1, 5, 30, 4, 9]

            ---
            mid point location: 2

            index of max([10, 1, 5]) = 0
            index of max([1, 5]) = 2
            index of max([5]) = 2
            index of max([5, 30]) = 3
            index of max([5, 30, 4]) = 3
            index of max([5, 30, 4, 9]) = 3
            ---
            Then output = [0, 2, 2, 3, 3, 3]
    """

    mid_point = (len(evExcScores) - 1) // 2
    partial_max_list = [None]*len(evExcScores)

    for i in range(mid_point + 1):

        l_left = evExcScores[i:mid_point + 1]

        position_of_max_from_i_to_mid_point = evExcScores.index(max(l_left))

        partial_max_list[i] = position_of_max_from_i_to_mid_point

    for j in range(mid_point + 2, len(evExcScores) + 1):

        l_right = evExcScores[mid_point + 1:j]

        position_of_max_from_mid_point_to_j = evExcScores.index(max(l_right))

        partial_max_list[j - 1] = position_of_max_from_mid_point_to_j

    return partial_max_list


def index_of_most_exciting_ev_within_interval(tic, toc, db):
    """ Given first index and last index search the database for the location of
        the maximum value
    """
    evExcScores = db['evExcScores']
    mid_point = (tic + toc) // 2

    partial_max_list, offset = db[mid_point]

    ind_max1 = partial_max_list[tic - offset] + offset
    ind_max2 = partial_max_list[toc - offset] + offset

    if evExcScores[ind_max1] > evExcScores[ind_max2]:
        index = ind_max1
    else:
        index = ind_max2

    return index

def create_db_for_quick_max_search(evExcScores):
    """ Create a n logn in size database for quick search of max
    """

    db = dict()
    db['evExcScores'] = evExcScores

    n = len(evExcScores)

    # max possible mid point having first equals to zero is (n-1)//2
    for mid_point_first_half in range((n - 1) // 2 + 1):
        last = 2 * mid_point_first_half + 1
        partial_max_list = _create_partial_max_lists(evExcScores[:last+1])
        db[mid_point_first_half] = (partial_max_list, 0)

    # min possible mid point having last equals to n-1 is (n-1)//2 + 1
    for mid_point_second_half in range((n - 1) // 2 + 1, n):
        first = mid_point_second_half - (n - 1 - mid_point_second_half)
        partial_max_list = _create_partial_max_lists(evExcScores[first:])
        db[mid_point_second_half] = (partial_max_list, first)

    return db

def _test_create_partial_max_lists():
    assert [0, 2, 2, 3, 3, 3] == _create_partial_max_lists([10, 1, 5, 30, 4, 9]),\
        'Bad partial max list creation'
    assert [0] == _create_partial_max_lists([10]),\
        'Bad partial max list creation'
    assert [] == _create_partial_max_lists([]),\
        'Bad partial max list creation'

def _test_create_db_for_quick_max_search():
    aList = [10, 1, 5, 30, 4, 9]
    db = create_db_for_quick_max_search(aList)

    def print_dict(offset, dic):
        """[summary]

        [description]

        Arguments:
            offset {[type]} -- [description]
            dic {[type]} -- [description]
        """

        for item in dic.items():
            print(offset*' '+'{}='.format(item[0]), end='')
            if isinstance(item[1], dict):
                print('\n'+offset*' '+'{')
                print_dict(offset+2, item[1])
                print(offset*' '+'}')
            else:
                print(item[1])

    print('Check the DB for', aList)
    print('-------')
    print_dict(4, db)
    print('-------')

def _test_index_of_most_exciting_ev_within_interval():
    aList = [10, 1, 5, 30, 4, 9]
    db = create_db_for_quick_max_search(aList)

    assert 3 == index_of_most_exciting_ev_within_interval(0, len(aList)-1, db),\
        'Wrong index'
    assert 0 == index_of_most_exciting_ev_within_interval(0, 2, db),\
        'Wrong index'

if __name__ == '__main__':
    _test_create_partial_max_lists()
    _test_create_db_for_quick_max_search()
    _test_index_of_most_exciting_ev_within_interval()
    print('All good')
