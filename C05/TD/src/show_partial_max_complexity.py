#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import partial_max as pm
import partial_max as pm
import matplotlib as mpl
import matplotlib.pyplot as plt
import time
import sys
import numpy as np

def main():
    import random

    N = 30
    exp = 12

    print('Creating {} DBs of sizes {} to 2^{}...'.format(N, int(1./N*2**exp), exp))

    dbSizes = [None]*N
    times = [None]*N
    evSizes = [int((i+1)/(N*1.)*2**exp) for i in range(N)]

    for idx, siz in enumerate(evSizes):

        evExcScores = random.sample(range(0, 10*siz), siz)

        # gen db
        evDB = pm.create_db_for_quick_max_search(evExcScores)

        # chose two random points in time | i < j
        i = 0
        j = len(evExcScores)-1
        # i, j = sorted(random.sample(range(0, len(evExcScores)), 2))

        # time the search of the max
        tic = time.time()
        ind = pm.index_of_most_exciting_ev_within_interval(i, j, evDB)
        times[idx] = time.time() - tic

        dbSizes[idx] = pm.getDBSize(evDB)

    ## Plot runtime
    plt.figure()
    plt.scatter(evSizes, [1000.*t for t in times])
    plt.plot(evSizes, [1000.*t for t in times], linewidth=0.5, label='O(log(n))')
    plt.legend(loc=4)
    plt.grid('on')
    plt.ylabel('time (ms)')
    plt.xlabel('input size N')
    plt.title('Runtime')

    ## Plot storage usage
    plt.figure()
    plt.scatter(evSizes, [i/1000. for i in dbSizes])
    plt.plot(evSizes, [i/1000. for i in dbSizes], linewidth=0.5, label='O(nlog(n)) aka quasi-linear')

    # include a linear plot as reference
    m = (dbSizes[1] - dbSizes[0])/(evSizes[1] - evSizes[0])
    # m = sys.getsizeof(evExcScores[0])
    plt.plot(evSizes, [m*i/1000. for i in evSizes], linewidth=0.5, label='linear', c='g')
    plt.scatter(evSizes, [m*i/1000. for i in evSizes], linewidth=0.5, c='g')

    # O(n log(n))
    # plt.plot(evSizes, [m*i*np.log2(m*i) for i in evSizes], linewidth=0.5, label='True O(nlogn)', c='r')
    # plt.scatter(evSizes, [m*i*np.log2(m*i) for i in evSizes], linewidth=0.5, c='r')

    # Quadratic
    # plt.plot(evSizes, [m*i*m*i for i in evSizes], linewidth=0.5, label='linear', c='purple')
    # plt.scatter(evSizes, [m*i*m*i for i in evSizes], linewidth=0.5, c='purple')

    plt.legend(loc=4)
    plt.grid('on')
    plt.ylabel('Size in kilobytes')
    plt.xlabel('input size N')
    plt.title('Storage')

    plt.show()

if __name__ == '__main__':
    main()
