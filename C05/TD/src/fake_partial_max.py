import time

def index_of_most_exciting_ev_within_interval(tic, toc, db):
    le_max = db[tic]
    idx_max = tic
    for i, e in enumerate(db[tic:toc+1]):
        time.sleep(.0001)
        if e > le_max:
            idx_max = i + tic
            le_max = e
    return idx_max

def create_db_for_quick_max_search(evExcScores):
    return evExcScores

def _test_partial_max():
    pass

if __name__ == '__main__':
    _test_partial_max()
