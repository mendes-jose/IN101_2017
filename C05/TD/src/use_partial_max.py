#!/usr/bin/env python
# -*- coding: utf-8 -*-

import partial_max as pm
import time
def main():
    import random

    # create the chronologicaly ordered set of events' excitement score
    n = 1024
    evExcScores = random.sample(range(0, 2*n), n)

    print('Creating DB for input size {}...'.format(n))
    # create the O(nlogn) data base
    evDB = pm.create_db_for_quick_max_search(evExcScores)

    # chose two random points in time | i < j
    # i, j = 0, n-1
    i, j = sorted(random.sample(range(0, len(evExcScores)), 2))

    # find the index of the most exciting sports event within a time interval (in O(1) time)
    tic = time.time()
    ind = pm.index_of_most_exciting_ev_within_interval(i, j, evDB)
    print('Time to find in DB', 1000.*(time.time()-tic), 'miliseconds')
    print('The most exciting event between dates {} and {} is the {}th of the series with score {}'.format(i, j, ind, evExcScores[ind]))
    print('Which should match index: {} and score: {} found with built-in methods'.format(evExcScores.index(max(evExcScores[i:j+1])), max(evExcScores[i:j+1])))

if __name__ == '__main__':
    main()
