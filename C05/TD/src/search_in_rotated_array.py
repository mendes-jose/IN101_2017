def search_in_rotated_array(aList, item, k=None):
    """ Has to be O(log(n))
    """
    from binary_search import binary_search

    if k != None:

        # if no rotation, search the whole list
        if k == 0:
            position = binary_search(aList, item)

        # otherwise, compare to element at 0 and decide which list to search
        elif item >= aList[0]:
            position = binary_search(aList[:k+1], item)
        else:
            position = binary_search(aList[k:], item)
            if position != None:
                position += k

    else:
        # find k
        k = _find_number_of_rotations(aList)
        position = search_in_rotated_array(aList, item, k)

    return position

def _find_number_of_rotations(rotList):
    first = 0
    last = len(rotList) - 1

    while first <= last:
        midpoint = (first + last) // 2

        # if rotList[midpoint] < rotList[0] then k < m, so last become m - 1
        if rotList[midpoint] < rotList[0]:
            last = midpoint - 1

        else:
            first = midpoint + 1

        # print(first, last)

    k = first # or k = last + 1

    # Rotation of len(rotList) steps is the same as no rotation at all
    if k == len(rotList):
        k = 0

    return k

def _test_search():
    assert search_in_rotated_array([5, 9, 10, 1, 4], 9) == 1, \
        'Wrong position'

    assert search_in_rotated_array([1, 4, 5, 9, 10], 1) == 0, \
        'Wrong position'

    assert search_in_rotated_array([1, 4, 5, 9, 10], 11) == None, \
        'Wrong position'

if __name__ == '__main__':
    _test_search()
    print('All good')
