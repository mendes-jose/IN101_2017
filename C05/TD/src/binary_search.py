def binary_search(alist, item):
    """ Search of an item on a list

        Expects a list and item that can be compared to all elements in the list
    """


    # test alist for attribute len, if not present raise a TypeError
    if not hasattr(alist, '__len__'):
        raise TypeError('first input does not have attribute __len__')

    # test validity of inputs, if not valid return None
    if alist == [] or item == None:
        return None

    first = 0
    last = len(alist) - 1
    position = None
    while first <= last and position == None:
        midpoint = (first + last) // 2
        if alist[midpoint] == item:
            position = midpoint
        else:
            if item < alist[midpoint]:
                last = midpoint - 1
            else:
                first = midpoint + 1
    return position


def test_binary_search():

    assert binary_search([], 3) == None, \
        'binary_search on a empty list should return None'

    assert binary_search([1,2], None) == None, \
        'binary_search of None should return None'

    assert binary_search([], None) == None, \
        'binary_search of None should return None'

    assert binary_search([1], 1) == 0, \
        'binary_search of 1 in a 1 should return None'

    assert binary_search([1,2], 3) == None, \
        'binary_search of 3 in [1,2] should return None'

    aList = [1, 2, 3, 8, 9, 36, 87]
    item = 9
    its_position = 4
    assert binary_search(aList, item) == its_position, \
        '{} is at the position {} in the list {}'.format(item, its_position, aList)

    try:
        # try something that will raise my TypeError
        assert binary_search(1, 1) == 0
    except TypeError as te:
        print('My intended exception occurred, value:', te)


if __name__ == '__main__':
    test_binary_search()
    print('All good')
