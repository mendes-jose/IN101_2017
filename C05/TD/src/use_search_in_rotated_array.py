#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from search_in_rotated_array import search_in_rotated_array

def _gen_rotated_array(a, b, siz):
    """ Generate sorted random array of given size with unique elements in [a,b]
        interval and rotate it to the right a random number of times
    """
    aList = random.sample(range(a, b), siz)

    aList = sorted(aList)

    # generate the number of rotations
    k = random.randint(0, len(aList)-1)
    print('secret k', k)

    # rotate list k times to the right (in place op)
    aList[:] = aList[-k:] + aList[:-k]

    return aList

def main():
    import random

    aList = _gen_rotated_array(1, 15, 10)
    print ('The list:', aList)

    item = random.randint(1, 10)
    print ('Searched item:', item)

    pos = search_in_rotated_array(aList, item)
    print ('Its position:', pos)

if __name__ == '__main__':
    main()
