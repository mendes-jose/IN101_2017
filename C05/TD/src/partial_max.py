import sys

def _create_partial_max_lists(evExcScores):
    """ Given a list it creates a two partial maxima lists from 0 to mid point
        and from mid point to the end of the list.

        Ex :

            input: [10, 1, 5, 30, 4, 9]

            ---
            mid point location: 2

            index of max([10, 1, 5]) = 0
            index of max([1, 5]) = 2
            index of max([5]) = 2
            index of max([5, 30]) = 3
            index of max([5, 30, 4]) = 3
            index of max([5, 30, 4, 9]) = 3
            ---
            Then output = [0, 2, 2, 3, 3, 3]
    """

    mid_point = (len(evExcScores) - 1) // 2
    partial_max_list = [None]*len(evExcScores)

    for i in range(mid_point + 1):

        l_left = evExcScores[i:mid_point + 1]

        position_of_max_from_i_to_mid_point = evExcScores.index(max(l_left))

        partial_max_list[i] = position_of_max_from_i_to_mid_point

    for j in range(mid_point + 2, len(evExcScores) + 1):

        l_right = evExcScores[mid_point + 1:j]

        position_of_max_from_mid_point_to_j = evExcScores.index(max(l_right))

        partial_max_list[j - 1] = position_of_max_from_mid_point_to_j

    return partial_max_list


def index_of_most_exciting_ev_within_interval(tic, toc, db):
    """ Given first index and last index search the database for the location of
        the maximum value
    """
    if toc <= tic:
        raise ValueError

    evExcScores = db['evExcScores']
    mid_point = (tic + toc) // 2

    rec_db = db['db_root']

    keep_going = True

    while keep_going and rec_db != None:

        first = rec_db['first']
        last = rec_db['last']
        pml = rec_db['partial_max_list']
        db_mid_point = (first + last) // 2

        if tic <= db_mid_point and toc > db_mid_point:
            ind_max1 = pml[tic - first] + first
            ind_max2 = pml[toc - first] + first
            keep_going = False

        elif tic > db_mid_point:
            rec_db = rec_db['db_right']

        else:
            rec_db = rec_db['db_left']

    if evExcScores[ind_max1] > evExcScores[ind_max2]:
        index = ind_max1
    else:
        index = ind_max2

    return index

def _create_db_for_quick_max_search_rec(evExcScores, first, last):

    if last - first <= 1:
        return {'db_left': None, 'db_right': None, 'partial_max_list': [0], 'first': first, 'last': last}

    else:
        mid_point = (first + last) // 2

        pml = _create_partial_max_lists(evExcScores[first:last+1])

        db_left = _create_db_for_quick_max_search_rec(evExcScores, first, mid_point)

        db_right = _create_db_for_quick_max_search_rec(evExcScores, mid_point, last)

        return {'db_left': db_left, 'db_right': db_right, 'partial_max_list': pml, 'first': first, 'last': last}

def create_db_for_quick_max_search(evExcScores):
    """ Create a n logn in size database for quick search of max
    """

    db = dict()
    db['evExcScores'] = evExcScores # save the original list inside the DB

    # Create the recursive DB
    db['db_root'] = _create_db_for_quick_max_search_rec(evExcScores, 0, len(evExcScores) - 1)

    return db

# save size of DB
def getDBSize_rec(db):
    if db == None:
        return 0
    else:
        siz = sys.getsizeof(db['first'])
        siz += sys.getsizeof(db['last'])
        siz += sys.getsizeof(db['partial_max_list'])
        siz += sys.getsizeof('first')
        siz += sys.getsizeof('last')
        siz += sys.getsizeof('partial_max_list')
        siz += sys.getsizeof('db_left')
        siz += sys.getsizeof('db_right')
        siz += getDBSize_rec(db['db_left'])
        siz += getDBSize_rec(db['db_right'])
        return siz

def getDBSize(db):
    return sys.getsizeof(db['evExcScores']) + getDBSize_rec(db['db_root'])

def _test_create_partial_max_lists():
    assert [0, 2, 2, 3, 3, 3] == _create_partial_max_lists([10, 1, 5, 30, 4, 9]),\
        'Bad partial max list creation'
    assert [0] == _create_partial_max_lists([10]),\
        'Bad partial max list creation'
    assert [] == _create_partial_max_lists([]),\
        'Bad partial max list creation'

def _test_create_db_for_quick_max_search():
    aList = [10, 1, 35, 30]
    db = create_db_for_quick_max_search(aList)

    def print_dict(offset, dic):
        """[summary]

        [description]

        Arguments:
            offset {[type]} -- [description]
            dic {[type]} -- [description]
        """

        for item in dic.items():
            print(offset*' '+'{}='.format(item[0]), end='')
            if isinstance(item[1], dict):
                print('\n'+offset*' '+'{')
                print_dict(offset+2, item[1])
                print(offset*' '+'}')
            else:
                print(item[1])

    print('Check the DB for', aList)
    print('-------')
    print_dict(4, db)
    print('-------')

def _test_index_of_most_exciting_ev_within_interval():
    aList = [10, 1, 5, 30, 4, 9, 1425, 12, 57, 12]
    db = create_db_for_quick_max_search(aList)

    assert 6 == index_of_most_exciting_ev_within_interval(0, len(aList)-1, db),\
        'Wrong index'
    assert 0 == index_of_most_exciting_ev_within_interval(0, 2, db),\
        'Wrong index'

if __name__ == '__main__':
    _test_create_partial_max_lists()
    _test_create_db_for_quick_max_search()
    _test_index_of_most_exciting_ev_within_interval()
    print('All good')
