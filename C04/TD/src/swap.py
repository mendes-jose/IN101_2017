import array

def swap_ij(data, i, j):
    """ Swap two elements from the given positions

    Arguments:
         {array} -- data
         {int} -- i
         {int} -- j
    """

    assert hasattr(data, '__iter__') == True, "data is not iterable"
    assert i >=0 and i < len(data), "i is out of limits"
    assert j >=0 and j < len(data), "j is out of limits"

    # swap_ij
    data[i], data[j] = data[j], data[i]

def _test_swap_ij():
    """ Test swap_ij
    """

    l = array.array('i', [0,1,2,3,4])
    swap_ij(l, 0, 4)
    assert (l == array.array('i', [4,1,2,3,0])), "Swap on array failed."

    try:
        swap_ij(l, 0, -1)
    except AssertionError:
        # Our invalid call caused an error. That's a good thing.
        # print('swap_ij does not take negative indexes')
        pass

if __name__ == "__main__":
    _test_swap_ij()
    print("All good")
