from random import randint
from array import array

def make_random_array(length, a, b):
    """generate an array of a certain length, with values in the range [a-b]

    --inputs: 
    	length  - lenght of the array (may not be smaller than 0)
        a, b    - range from which random integers are sampled (with a<=b)
    --output:
    	returns an array (of specified length) with random numbers

    remark: if an error occurs, an empty list is returned
    remark: if length==0, there is no error, but an empty list is returned
    """

    assert length >= 0    , "make_random_array: length must be >= 0"
    assert b>=a, "make_random_array: b must be greater than a"

    return array("i", [randint(a, b) for i in range(0, length)])



def _test_make_random_array():
    # Test for various lengths (some good, some bad)
    for length in range(0,10):
        data = make_random_array(length, 0, 10)
        print("make_random_array(",length,") returns:", data)
        assert len(data) == length, "Invalid array length returned"
        
        
    # Here come an advanced trick to catch assertion errors
    try:
        # This should cause an assertion error, which is caught below.
        data = make_random_array(-1,0,1)
        print("The line above should have throw an AssertionError, but it didn't.")
    except AssertionError:
        # Our invalid call caused an error. That's a good thing.
        pass
    
    # Here come an advanced trick to catch assertion errors
    try:
        # This should cause an assertion error, which is caught below.
        data = make_random_array(2,2,1)
        print("The line above should have throw an AssertionError, but it didn't.")
    except AssertionError:
        # Our invalid call caused an error. That's a good thing.
        pass
    


if __name__ == '__main__':
    _test_make_random_array()


