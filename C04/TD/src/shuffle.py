from array import array
from random_array import make_random_array
from swap import swap_ij

def shuffle(data):
    """ Shuffle an array

    Arguments:
         {array} -- data
    """

    assert hasattr(data, '__iter__') == True, "data is not iterable"

    randArray = make_random_array(len(data), 0, len(data)-1)

    for i, j in zip(range(len(data)), randArray):
        swap_ij(data, i, j)

def _test_shuffle():
    """ Test swap_ij
    """

    arr = array('i', range(10, 30))
    shuffle(arr)
    assert arr != array('i', range(10, 30)), "Shuffle on array failed."

if __name__ == "__main__":
    _test_shuffle()
    print("All good")
