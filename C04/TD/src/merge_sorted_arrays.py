import random
from array import array

def merge_sorted_arrays(left,right):
    """ merges two sorted arrays into one sorted array

    parameters:
      left, right : both arrays that are sorted in ascending order
      left,right CAN be empty

    returns:
      a single sorted array

    potential error conditions:
      left, right are not arrays or do not contain numerical values (NOT CHECKED)
    """

    len_new = len(left)+len(right) # Length of the merged array
    result = array("i",[0]*(len_new)) # The result will be of this length

    # These indices will move from the back to the front of each of the three
    # arrays.
    index_left=0
    index_right=0
    index_result=0

    # Continue until we've reached the end of both arrays
    while index_left<len(left) or index_right<len(right):

        if index_left==len(left):
            # We are done with 'left'. Fill 'result' with 'right'
            result[index_result] = right[index_right]
            index_right+=1

        elif index_right==len(right):
            # We are done with 'right'. Fill 'result' with 'left'
            result[index_result] = left[index_left]
            index_left+=1

        else:
            if left[index_left] < right[index_right]:
                # Current value in 'left' is smaller than that in 'right'
                # Therefore, put this value in 'result' and proceed.
                result[index_result] = left[index_left]
                index_left+=1

            else:
                # Current value in 'right' is smaller than that in 'left'
                # Therefore, put this value in 'result' and proceed.
                result[index_result] = right[index_right]
                index_right+=1

        # We've either added a value from 'left' or 'right' to 'result'.
        # Therefore, we must move to the next index in result.
        index_result+=1

    return result


def _test_merge_sorted_arrays():

    x = array("i",[0,1])
    y = array("i",[2,5,7,8])
    result = merge_sorted_arrays(x,y)
    print("array 1", x)
    print("array 2", y)
    print("merged ", result)
    assert result == array("i",[0,1,2,5,7,8]), "incorrect result"

    empty1 = array("i",[])
    empty2 = array("i",[])
    assert merge_sorted_arrays(empty1,empty2) == array("i",[]), "Incorrect result!"

    assert merge_sorted_arrays(x,empty1) == x, "Incorrect result"
    print('All tests were successful!')



if __name__ == '__main__':
    _test_merge_sorted_arrays()
