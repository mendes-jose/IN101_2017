from array import array
from random import randint
from swap import swap_ij

def shuffle(data):
    """ Shuffle an array (Durstenfeld's algorithm)

    Arguments:
         {array} -- data
    """

    assert hasattr(data, '__iter__') == True, "data is not iterable"


    for i in range(len(data)-1):
        swap_ij(data, i, randint(i+1, len(data)-1))

def _test_shuffle():
    """ Test swap_ij
    """

    arr = array('i', range(10, 30))
    shuffle(arr)
    assert arr != array('i', range(10, 30)), "Shuffle on array failed."

if __name__ == "__main__":
    _test_shuffle()
    print("All good")
    # arr = array('i', range(10, 30))
    # print(arr)
    # shuffle(arr)
    # print(arr)
