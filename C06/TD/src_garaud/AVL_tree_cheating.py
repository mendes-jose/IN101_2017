
# si on avait les classes : 
# class AVL_tree():
#     "Cheating: this is not an AVL_tree, but has at least the correct interface"
#     def push_value(self,v):
#         pass


"""A *fake* AVL_tree , just a *sorted list* !

Does not have the correct performances, but has the right syntax.
"""

AVL_tree=list

def push_value(tree, v): 
    tree.append(v)
    tree.sort()
def pop_min(tree):
    return tree.pop(0)
def pop_max(tree):
    return tree.pop()  #  pop last by default
