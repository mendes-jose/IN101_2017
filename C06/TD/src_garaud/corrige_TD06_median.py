"""
Idée générale : on stocke : 
 - la valeur médiane m
 - le tableau S (smaller) des valeurs strictement plus petites que m
 - le tableau T (taller) des valeurs strictement plus grandes que m

Cette structure a les propriétés suivantes (invariants) : 
 - pour tout s in S : s<m
 - pour tout t in T : t>m
 - len(T) = len (S) + (0 ou 1)
Pour la vérification, on peut s'écrire une fonction check_invariants(m,S,T). 

Pour ajouter une valeur, on la place dans S ou T, puis si besoin on rééquilibre la structure (pour retrouver les invariants).

La performance demandée ( O(log(N)) ) nécessite qu'on choisisse des AVL tree pour S et T (ou min/max_heap)

On suppose ici qu'on a à disposition un AVL_tree qui a les fonctions : 
  - push_value (lui ajoute une valeur, performant en O(log(N))
  - pop_min/max (cherche et sort le min/max, performant en O(log(N))
"""

from AVL_tree_cheating import AVL_tree, push_value, pop_min, pop_max
# on pourra ensuite coder notre propre AVL_tree, 
# ou utiliser par ex : https://pypi.python.org/pypi/bintrees/2.0.2

def insert_value(cnew, m, S, T): 
    "Ajoute la nouvelle valeur dans la structure composée de (m,S et T)"
    if cnew < m: 
        push_value(S, cnew)
    else: 
        push_value(T, cnew)

    # reequilibrage : 
    if len(T) - len(S) in [0,1]: 
        pass  # rien a faire, la structure est bien équilibrée
    elif len(T) - len(S) == -1 :  # S est trop "lourde"
        push_value(T,m)
        m = pop_max(S)
    elif len(T) - len(S) == 2 : # T est trop "lourde"
        push_value(S,m)
        m = pop_min(T)
    else: 
        assert False, "Ne devrait jamais arriver"

    return m,S,T  # pour etre certain de bien recuperer les modifications
    # return m    # suffirait
    # de toutes façons, avec les classes, ce serait beaucoup plus joli


"""
Analyse de complexité : 
un appel à insert_value coûte : 
 - 1 comparaison (l. 28)
 + 1 ajout (l. 29 ou 31)
 - des appels à len (ici 6 appels, peut être facilement ramené à 2)
 + 1 ajout (l. 37 ou 40)
 + 1 pop_min ou max (l. 38 ou 41)

Les opérations avec un "-" sont à coût constant O(1). 
Les opérations avec un "+" sont plus chères :
  push_value coûte O(log(N/2))
  pop_min/max coûte O(log(N/2))
Donc au total, 3*O(log(N/2)) + O(1) = O(log(N))
"""


def check_invariants(m,S,T): 
    """Vérifie les 3 invariants de notre structure. 
    En phase de preuve/tests cette fonction doit être appelée après chaque insertion.
    
    Attention, cette fonction à un coût en O(N) (donc en production, la supprimer)
    """
    assert (len(T) - len(S)) in [0,1]
    # si on préfère : assert (len(S) == len(T)) or (len(S)+1 == len(T))
    for s in S:
        assert (s<m)
    for t in T:
        assert (t>m)
        



# exemple d'utilisation : 
m = 7
S = AVL_tree([2,3])
T = AVL_tree([12,15])
# valeurs : 2,3, 7, 12,15
check_invariants(m,S,T)
print (S, m, T)

m,S,T = insert_value(9, m, S, T) 
# valeurs : 2,3, 7, 9,12,15
# ici, 7 est tjs la mediane, 9 est dans T : 
assert (m == 7)
assert (9 in T)
check_invariants(m,S,T)
print (S, m, T)

m,S,T = insert_value(10, m, S, T) 
# valeurs : 2,3,7, 9, 10,12,15
# ici, la mediane a ete poussee dans S, 9 est la nouvelle mediane : 
assert (m == 9)
assert (7 in S)
assert (len(S) == 3) 
check_invariants(m,S,T)
print (S, m, T)

m,S,T = insert_value(0, m, S, T) 
check_invariants(m,S,T)
print (S, m, T)
m,S,T = insert_value(1, m, S, T) 
check_invariants(m,S,T)
print (S, m, T)
m,S,T = insert_value(4, m, S, T) 
check_invariants(m,S,T)
print (S, m, T)



# """ ATTENTION:
# Je n'ai pas vérifié comment le code réagit si on met tout le temps la même valeur :
# """
# m = 7
# S = AVL_tree([2,3])
# T = AVL_tree([12,15])
# check_invariants(m,S,T)
# print (S, m, T)

# insert_value(7,m,S,T)
# insert_value(7,m,S,T)
# insert_value(7,m,S,T)
# insert_value(7,m,S,T)
# insert_value(7,m,S,T)
# insert_value(7,m,S,T)
# insert_value(7,m,S,T)
# print (S, m, T)
# check_invariants(m,S,T)



# test un peu plus sérieux:
from random import randint
for i in range(100): 
    v = randint(0,1000)
    m,S,T = insert_value(v, m, S, T) 
    check_invariants(m,S,T)
print (S,m,T)
