"""Idée générale: 
  - on construit progressivement la réponse au problème sous forme d'une table
  KS(n,w) = le sous-problème de placer les n meilleurs items dans un sac de contenance w (n<N et w<W)

  (Notation: N est le nb total d'items et W le poids total autorise)

  Le meilleur choix possible pour ajouter (ou non) le n-ième item s'écrit : 
  
  KS(n, w) = max ( KS(n-1, w) , 
                   KS(n-1, w-w(n)) + val(n))
  (formule compliquée, mais elle cache juste que le n-ième item : 
    - soit on le laisse, et la meilleure solution avec n-1 éléments est la bonne
    - soit on le prend, il consomme w(n) kilos, et le reste du sac à dos est optimisé avec n-1 items dans w-w(n) kilos

"""

values = [7, 9, 5, 12, 14, 6, 12]
weights= [3, 4, 2,  6,  7, 3,  5]

# attention: sans memoization, on n'aurait pas la perf en O(N*W)
KS_memo={}

def KS_recursive(n, w):
    if (n,w) in KS_memo.keys(): 
        return KS_memo[(n,w)]
    if (n == 0):
        val=0
    else :
        if (weights[n-1] > w) : # impossible d'ajouter l'objet, il est trop lourd
            val = KS_recursive(n-1,w)
        else: 
            val = max( KS_recursive(n-1,w) , values[n-1] + KS_recursive(n-1, w-weights[n-1]))
    KS_memo[(n,w)] = val
    return val

def KS(W): 
    return KS_recursive(len(values), W)

for i in range(10): 
    print (i, KS(i))


"""
Complexité : 
assez difficile à appréhender en récursif. 
La matrice qu'on avait au tableau a N*W termes, chacun étant obtenu par un nombre fini d'opérations élémentaires (ci-dessus)
On va la vérifier par tests, ci-dessous. 
Voir aussi knapsack2.py, on on fait l'analyse proprement.
"""


# perf test 
from random import randint
import time
def perf_it(N):
    global KS_memo, values, weights
    KS_memo={}  # on vide la memoire avant chaque utilisation
    values = [ randint(0,1000) for _ in range(N) ]
    weights= [ randint(0,1000) for _ in range(N) ]

    t0 = time.time()
#    print (KS(1000))
    KS(1000)
    t1 = time.time()
    print (N, t1-t0)

for i in range(20):
    perf_it(100*i)

