"""Idée générale: 
  - on construit progressivement la réponse au problème sous forme d'une table
  KS(n,w) = le sous-problème de placer les n meilleurs items dans un sac de contenance w (n<N et w<W)

  (Notation: N est le nb total d'items et W le poids total autorise)
  Note : on tire parti du fait que les poids sont entiers, donc w n'a que W valeurs possibles

  L'ajout du n-ième item de façon optimale s'écrit : 
  
  KS(n, w) = max ( KS(n-1, w) , KS(n-1, w-w(n)) + val(n))
  (formule compliquée, mais elle cache juste que le n-ième item : 
    - soit on le laisse, et la meilleure solution avec n-1 éléments est la bonne
    - soit on le prend, il consomme w(n) kilos, et le reste du sac à dos est optimisé avec n-1 items dans w-w(n) kilos
"""


def KS_bottom_up(N,W):
    global KS_array, values, weights
    for n in range(0,N+1):
        for w in range(0,W+1):
            if (n == 0):
                val=0
            else :
                if (weights[n-1] > w) : # impossible d'ajouter l'objet, il est trop lourd
                    val = KS_array[(n-1,w)]
                else: 
                    val = max( KS_array[(n-1,w)] , values[n-1] + KS_array[(n-1, w-weights[n-1])])
            KS_array[(n,w)] = val
    return KS_array[(N,W)]

def KS(W): 
    return KS_bottom_up(len(values),W)


"""
Evaluation de la complexité : 
 - le tableau complet est de taille W*N 
 - pour remplir chaque case, j'ai un nombre borné d'opérations élémentaires (comparaisons, max, extraire des valeurs connues dans le tableau KS_array, addition) donc O(1) par case
 - au total, j'ai une complexité en O(W*N)
"""



# TESTS 

values = [7, 9, 5, 12, 14, 6, 12]
weights= [3, 4, 2,  6,  7, 3,  5]
KS_array={}
for i in range(1,16):
    print (i, KS(i))


# on vérifie la complexité en O(N*W) : 
from random import randint
import time
WEIGHT=1000

def perf_it(N):
    global KS_array, values, weights
    KS_array={}  # on vide la memoire avant chaque utilisation
    values = [ randint(0,1000) for _ in range(N) ]
    weights= [ randint(0,1000) for _ in range(N) ]

    t0 = time.time()
#    print (KS(1000))
    KS(WEIGHT)
    t1 = time.time()
    print (N, t1-t0)

for i in range(1,40):
    perf_it(100*i)

