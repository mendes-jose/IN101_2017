# Input: number of terms to use in approximation
n = 19

# Algorithm (version 2: uses exactly n terms, even if n is uneven)
# Also prints the individual terms

# Compute pi incrementally, starting with 0.0
pi_approx = 0.0

# Add terms with denominators 1,5,9,13 etc.
for ii in range(1, 2*n, 4):
    pi_approx = pi_approx + (4.0/ii)  # Add the term 4/ii
    print('+4/',ii)      # Print this term also
    
# Subtract terms with denominators 3,7,11,15 etc.
for ii in range(3, 2*n, 4): 
    pi_approx = pi_approx - (4.0/ii)  # Subtract the term 4/ii
    print('-4/',ii)      # Print this term also

# Output
print('pi is approximately',pi_approx)
