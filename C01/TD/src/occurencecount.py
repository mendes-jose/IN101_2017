# Input
a = 2                        # Number to search for 
l = [1, 8, 3, 2, 7, 2, 7, 2] # A list of numbers

# Algorithm
occurence_count = 0
for item in l:
    if (a==item):
        # If this item in the list is equal to 'a', then
        # the occurence counter is incremented by 1 
        occurence_count = occurence_count+1

# Output: number of times 'a' occurs in 'l'
# For instance, in the list above, '2' occurs 3 times 
print(occurence_count)
