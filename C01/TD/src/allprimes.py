# Input
up_to_number = 21

print('Here comes a list of prime numbers up to: {}'.format(up_to_number))

# Algorithm
for number in range(2, up_to_number + 1):

    # Assume the number is prime until proven otherwise
    isprime = True

    # Try to divide the number by all divisor 2..(number-1)
    # If we can divide it with one of the divisors (which
    # means the remainder of the division is 0), it is not a prime
    # Once we know it is not a prime, we can stop testing with
    # the other divisors
    divisor = 2
    while divisor < number and isprime:
        # Short version with the modulo operator %
        if (number%divisor==0): # does divisor divide number without a remainder?
            isprime = False     # if so, it cannot be a prime
        divisor += 1  # short way of saying divisor = divisor + 1

    # Output
    if isprime:
        print(number)
