# Input
number = 21

# Check if the number is valid
if (number<2):
    print('Please use a number larger than 1! Results may be unexpected...')

# Algorithm

# Assume the number is prime until proven otherwise
isprime = True

# Rest of the algorithm follows...

# Try to divide the number by all divisor 2..(number-1) 
# If we can divide it with one of the divisors (which 
# means the remainder of the division is 0), it is not a prime
# Once we know it is not a prime, we can stop testing with the other divisors
divisor = 2
while divisor<number and isprime:

    # Compute: remainder = number mod divisor
    remainder = number
    while (remainder >= divisor):
        remainder = remainder - divisor

    # If divisor divides number without a
    # remainder, then number is not a prime
    if (remainder == 0):
        isprime = False
        
    # The above can be done much shorter when using the modulo operator %
    # The code block would then simply be: 
    # if (number%divisor==0):
    #     isprime = False
    # This would be the preferred version, but % wasn't presented yet...

    divisor += 1  # short way of saying divisor = divisor + 1
    
# Output
if isprime:
    print(number,'is a prime number!')
else:
    print(number,'is not a prime number.')
