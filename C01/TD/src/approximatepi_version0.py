# Make sure to use python3; will not work with python2

# Input: number of terms to use in approximation
n = 11

# Algorithm

# Compute pi incrementally, starting with 0.0
pi_approx = 0.0

for ii in range(n): # Generates 0,1,2,3,4 etc.
    numerator   = 4 * (-1)**ii  # -1^ii generates 1,-1,1,-1,1 etc.
    denominator = 2*ii + 1      # generates 1,3,5,7,etc
    pi_approx   = pi_approx + numerator/denominator 
    # Print all the terms, one by one (not necessary, but helps to debug)
    print('term',(ii+1),'=',numerator,'/',denominator)

# Output
print('pi is approximately',pi_approx)
