# Input: number of terms to use in approximation
n = 20 # for this version, n must be even!

# Algorithm (version 1: simpler, but expects n to be even)

# Compute pi incrementally, starting with 0.0
pi_approx = 0.0

for ii in range(1, 2*n, 4): # Generates 1,5,9,13 etc.
    # Compute the next two terms, i.e. +4/ii and -4/(ii+2)
    pi_approx = pi_approx + (4.0/ii) - (4.0/(ii+2))

# Output
print('pi is approximately',pi_approx)
