print('First for loop')
for number in [0, 1, 2, 3, 4]:
    print(number)

print('Second for loop')
for number in range(5):
    print(number)

print('Third for loop')
for number in range(4, 11):
    print(number)

print('Fourth for loop')
for number in range(4,17,3):
    print(number)




print('First while loop')
number=0
while (number<5):
    print(number)
    number = number+1

print('Second while loop')
number=4
while (number<11):
    print(number)
    number = number+1

print('Fourth while loop')
number=4
while (number<17):
    print(number)
    number = number+3
