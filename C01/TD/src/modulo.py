# Input
dividend  = 26
divisor =  7

# Algorithm to compute 'dividend mod divisor'
#
# Main idea: subtract the divisor from the dividend
# until the dividend becomes smaller than the divisor.
# The number that is left is the remainder.
#
# Example:
#   26 - 7 -> 19
#   19 - 7 -> 12 
#   12 - 7 ->  5
# Can't subtract anymore, because 5 < 7 
# Therefore, 5 is the remainder

remainder = dividend
while (divisor < remainder):
    remainder = remainder - divisor 

# Output
print(remainder)
